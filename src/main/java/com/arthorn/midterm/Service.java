package com.arthorn.midterm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ACER
 */
public class Service {

    private static ArrayList<Product> proList = new ArrayList<>();

    static {
        load();
    }

    public static boolean addProduct(Product product) {
        proList.add(product);
        save();
        return true;
    }

    public static void delAll() {
        proList.removeAll(proList);
    }

    public static boolean delProduct(Product product) {
        proList.remove(product);
        save();
        return true;
    }

    public static boolean delProduct(int index) {
        proList.remove(index);
        save();
        return true;
    }

    public static ArrayList<Product> getProduct() {
        return proList;
    }

    public static Product getProduct(int index) {
        return proList.get(index);
    }

    public static boolean updatProduct(int index, Product product) {
        proList.set(index, product);
        save();
        return true;
    }

    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {

            file = new File("pro.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(proList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {

            file = new File("pro.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            proList = ((ArrayList<Product>) ois.readObject());
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static double price() {
        int price = 0;
        for (int i = 0; i < proList.size(); i++) {
            price = price + (proList.get(i).getPrice() * (proList.get(i).getAmount()));
        }
        return price;
    }

    public static int amount() {
        int amount = 0;
        for (int i = 0; i < proList.size(); i++) {
            amount = amount + proList.get(i).getAmount();
        }
        return amount;
    }

}
